#include "mgos.h"
#include "mgos_rpc.h"
#include "mgos_updater_common.h"
#include "mgos_http_server.h"


namespace smiley {
  static enum mgos_init_result smiley_init(void) {
    // Initialize pins.
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_right_eye_dot(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_left_eye_nose(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_mouth_o_eyes_hor(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_mouth_happy(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_mouth_sad(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_left_eye_diag(), MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_mode(mgos_sys_config_get_pins_leds_right_eye_diag(), MGOS_GPIO_MODE_OUTPUT);

    return MGOS_INIT_OK;
  }

  enum class Face {
    OFF,
    SMILE,
    ANGRY,
    SURPRISED,
    SAD,
    WINK
  };

  static void smile(enum Face face) {
    auto right_eye_dot = mgos_sys_config_get_pins_leds_right_eye_dot();
    auto left_eye_nose = mgos_sys_config_get_pins_leds_left_eye_nose();
    auto mouth_o_eyes_hor = mgos_sys_config_get_pins_leds_mouth_o_eyes_hor();
    auto mouth_happy = mgos_sys_config_get_pins_leds_mouth_happy();
    auto mouth_sad = mgos_sys_config_get_pins_leds_mouth_sad();
    auto left_eye_diag = mgos_sys_config_get_pins_leds_left_eye_diag();
    auto right_eye_diag = mgos_sys_config_get_pins_leds_right_eye_diag();

    LOG(LL_INFO, ("smile!  Turning off all leds."));
    
    mgos_gpio_write(right_eye_dot, false);
    mgos_gpio_write(left_eye_nose, false);
    mgos_gpio_write(mouth_o_eyes_hor, false);
    mgos_gpio_write(mouth_happy, false);
    mgos_gpio_write(mouth_sad, false);
    mgos_gpio_write(left_eye_diag, false);
    mgos_gpio_write(right_eye_diag, false);

    LOG(LL_INFO, ("smile! Will now turn on the leds for faceId: %d.", face));

    switch (face) {
      case Face::SMILE:
        mgos_gpio_write(left_eye_nose, true);
        mgos_gpio_write(right_eye_dot, true);
        mgos_gpio_write(mouth_happy, true);
        break;
      case Face::ANGRY:
        mgos_gpio_write(left_eye_nose, true);
        mgos_gpio_write(right_eye_dot, true);
        mgos_gpio_write(mouth_sad, true);
        mgos_gpio_write(left_eye_diag, true);
        mgos_gpio_write(right_eye_diag, true);
        break;
      case Face::SURPRISED:
        mgos_gpio_write(left_eye_nose, true);
        mgos_gpio_write(right_eye_dot, true);
        mgos_gpio_write(mouth_o_eyes_hor, true);
        break;
      case Face::SAD:
        mgos_gpio_write(left_eye_nose, true);
        mgos_gpio_write(right_eye_dot, true);
        mgos_gpio_write(mouth_sad, true);
        break;
      case Face::WINK:
        mgos_gpio_write(left_eye_nose, true);
        mgos_gpio_write(right_eye_diag, true);
        mgos_gpio_write(mouth_happy, true);
        break;
      case Face::OFF:
      default:
        break;
    }
  }
}

static void rpcSmile(struct mg_rpc_request_info *ri, void *cb_arg,
                     struct mg_rpc_frame_info *fi, struct mg_str args) {
  LOG(LL_INFO, ("RPC Smile json"));
  uint8_t face;
  if(json_scanf(args.p, args.len, ri->args_fmt, &face) == 1) {
    LOG(LL_INFO, ("  Changing face to: %d", face));
    smiley::smile((enum smiley::Face) face);
  }
  mg_rpc_send_responsef(ri, "true");

  LOG(LL_INFO, ("Smiley uptime: %.2lf, RAM: %lu, %lu free",
                mgos_uptime(), (unsigned long) mgos_get_heap_size(),
                (unsigned long) mgos_get_free_heap_size()));

  (void) cb_arg;
  (void) fi;
}

static void net_cb(int ev, void *evd, void *arg) {
  switch (ev) {
    case MGOS_WIFI_EV_STA_DISCONNECTED:
      LOG(LL_INFO, ("%s", "Net disconnected"));
      break;
    case MGOS_WIFI_EV_STA_CONNECTING:
      LOG(LL_INFO, ("%s", "Net connecting..."));
      break;
    case MGOS_WIFI_EV_STA_CONNECTED:
      LOG(LL_INFO, ("%s", "Net connected"));
      break;
    case MGOS_WIFI_EV_STA_IP_ACQUIRED:
      LOG(LL_INFO, ("%s", "Net got IP address"));
      break;
  }

  (void) evd;
  (void) arg;
}

enum mgos_app_init_result mgos_app_init(void) {
  // Publish to MQTT on button press
  // mgos_gpio_set_button_handler(mgos_sys_config_get_pins_button(),
  //                              MGOS_GPIO_PULL_UP, MGOS_GPIO_INT_EDGE_NEG, 20,
  //                              button_cb, NULL);

  // Network connectivity events
  mgos_event_add_group_handler(MGOS_EVENT_GRP_NET, net_cb, NULL);

  smiley::smiley_init();
  mg_rpc_add_handler(mgos_rpc_get_global(), "Smile.Set", "{face: %d}", rpcSmile, NULL);

  smiley::smile(smiley::Face::SMILE);

  mgos_upd_commit();
  
  return MGOS_APP_INIT_SUCCESS;
}
