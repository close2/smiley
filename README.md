# smiley

## Overview

Controls a car smiley (https://www.amazon.co.uk/Drivemocion-Smileys-Different-Remote-Control/dp/B0127HD69U)

I have lost the remote control.
Inside the smiley device are 2 ICs.  An Atmel microcontroller and a darlington-transistor array.

Fortunately the microcontroller was inside a socket and easily removed.  I then identified which
pins of the transistor array enabled which LED-block.

An esp8266 (wemos d1 mini) was attached to those pins and this project provides the software to
control the smiley using WIFI.


## License

This project is GPL because I use the GPL OTA libraries.

If you don't need them and want another LICENSE contact me.


## Building and installing

Download mos from the mongoose-os project.

Generate your own `fs/rpc_auth.txt` file using htdigest.  Or rename the example file.

Build the firmware: `mos build --arch esp8266`

Flash it: `MOS_PORT=/dev/ttyUSB5 mos flash`

Have fun.


